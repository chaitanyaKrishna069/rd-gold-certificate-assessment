package com.epam.rd.gold.certificate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.rd.gold.certificate.model.Batch;

public interface BatchRepository extends JpaRepository<Batch, Integer> {

}
