package com.epam.rd.gold.certificate.custom.exception;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.epam.rd.gold.certificate.dto.ExceptionResponse;

import jakarta.validation.ConstraintViolationException;

@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ExceptionResponse sendArgumentInvalidError(MethodArgumentNotValidException excpetion, WebRequest request) {
		List<String> errors = new ArrayList<>();
		excpetion.getAllErrors().forEach(error -> errors.add(error.getDefaultMessage()));
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(), errors.toString(),
				request.getDescription(false));
	}

	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ExceptionResponse sendArgumentInvalidError(ConstraintViolationException excpetion, WebRequest request) {
		List<String> errors = new ArrayList<>();
		excpetion.getConstraintViolations().forEach(error -> errors.add(error.getMessage()));
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(), errors.toString(),
				request.getDescription(false));
	}
	
	@ExceptionHandler(AssociateException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ExceptionResponse sendNotFoundEntryResponse(AssociateException exception, WebRequest request) {
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(), exception.toString(),
				request.getDescription(false));
	}
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ExceptionResponse sendInternalServerErrorIssue(Exception exception, WebRequest request) {
		return new ExceptionResponse(new Date().toString(), HttpStatus.INTERNAL_SERVER_ERROR.name(),
				exception.getMessage(), request.getDescription(false));
	}
	
	@ExceptionHandler(SQLIntegrityConstraintViolationException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ExceptionResponse sendIntegrityExcpetionResponse(SQLIntegrityConstraintViolationException exception,
			WebRequest request) {
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(), exception.toString(),
				request.getDescription(false));
	}
}