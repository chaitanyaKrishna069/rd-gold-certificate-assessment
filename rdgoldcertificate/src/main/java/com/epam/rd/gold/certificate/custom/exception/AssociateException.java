package com.epam.rd.gold.certificate.custom.exception;

public class AssociateException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8595259793154722452L;

	public AssociateException(String message) {
		super(message);
	}
}