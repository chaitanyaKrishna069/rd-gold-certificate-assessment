package com.epam.rd.gold.certificate.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.rd.gold.certificate.dto.AssociateDto;
import com.epam.rd.gold.certificate.dto.AssociateUpdateDto;
import com.epam.rd.gold.certificate.service.AssociateService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/rd/associates")
@RequiredArgsConstructor
public class AssociateController {

	private final AssociateService associateService;

	@PostMapping
	public ResponseEntity<AssociateDto> addAssociate(@RequestBody @Valid AssociateDto associateDto) {
		return new ResponseEntity<>(associateService.addAssociate(associateDto), HttpStatus.CREATED);
	}
	
	@GetMapping("/{gender}")
	public ResponseEntity<List<AssociateDto>> getAssociateByGender(@PathVariable String gender){
		return new ResponseEntity<>(associateService.getAssociatesByGender(gender), HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<AssociateUpdateDto> updateAssociateDetails(@RequestParam String email
			,@RequestBody @Valid AssociateUpdateDto associateUpdateDto){
		return new ResponseEntity<>(associateService.updateAssociateDetails(email, associateUpdateDto), HttpStatus.OK);
	}
	
	@DeleteMapping("{id}")
	public void deleteAssociateById(@PathVariable int id) {
		associateService.removeAssociateById(id);
	}
}