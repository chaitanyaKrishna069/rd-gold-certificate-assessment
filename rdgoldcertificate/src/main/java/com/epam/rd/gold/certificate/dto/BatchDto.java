package com.epam.rd.gold.certificate.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class BatchDto {
	@NotBlank(message = "The name should not be blank")
	@Size(min = 4, message = "The Batch name should be more than 4 characters")
	private String name;
	@NotBlank(message = "The practice name should not be blank")
	private String practic;
	@Pattern(regexp = "^([0-9]{2}/[0-9]{2}/[0-9]{4})$", message = "Date format should be dd/mm/yyyy")
	private String startDate;
	@Pattern(regexp = "^([0-9]{2}/[0-9]{2}/[0-9]{4})$", message = "Date format should be dd/mm/yyyy")
	private String endDate;
}