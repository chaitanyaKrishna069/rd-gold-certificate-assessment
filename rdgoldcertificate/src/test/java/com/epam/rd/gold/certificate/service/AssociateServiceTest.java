package com.epam.rd.gold.certificate.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.rd.gold.certificate.custom.exception.AssociateException;
import com.epam.rd.gold.certificate.dto.AssociateDto;
import com.epam.rd.gold.certificate.dto.AssociateUpdateDto;
import com.epam.rd.gold.certificate.model.Associate;
import com.epam.rd.gold.certificate.model.Batch;
import com.epam.rd.gold.certificate.repository.AssociateRepository;
import com.epam.rd.gold.certificate.repository.BatchRepository;

@ExtendWith(MockitoExtension.class)
class AssociateServiceTest {
	@Mock
	private AssociateRepository associateRepository;

	@Mock
	private BatchRepository batchRepository;

	@Mock
	private ModelMapper modelMapper;

	@InjectMocks
	private AssociateService associateService;

	@Test
	void should_addAssociate() {
		AssociateDto associateDto = new AssociateDto();
		Associate associate = new Associate();
		Batch batch = new Batch();
		Mockito.when(modelMapper.map(associate.getBatch(), Batch.class)).thenReturn(batch);
		Mockito.when(batchRepository.save(batch)).thenReturn(batch);
		Mockito.when(modelMapper.map(associateDto, Associate.class)).thenReturn(associate);
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);
		associateService.addAssociate(associateDto);
		Mockito.verify(associateRepository).save(associate);
	}

	@Test
	void should_Get_AssociateByGender() {
		Associate associate = new Associate();
		List<Associate> associates = new ArrayList<>();
		associates.add(associate);
		Mockito.when(associateRepository.findByGender("M")).thenReturn(associates);
		associateService.getAssociatesByGender("M");
		Mockito.verify(associateRepository).findByGender("M");
	}

	@Test
	void should_Update_AssociateByEmail() {
		AssociateUpdateDto associateDto = new AssociateUpdateDto();
		Associate associate = new Associate();
		Optional<Associate> optionalAssoiciate = Optional.of(associate);
		Mockito.when(associateRepository.findByEmail("cck@gmail.com")).thenReturn(optionalAssoiciate);
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);
		associateService.updateAssociateDetails("cck@gmail.com", associateDto);
		Mockito.verify(associateRepository).save(associate);
	}

	@Test
	void should_ThrowExcpetion_UpdateAssociateByMail() {
		AssociateUpdateDto associateDto = new AssociateUpdateDto();
		Mockito.when(associateRepository.findByEmail("cck@gmail.com")).thenReturn(Optional.empty());
		assertThrows(AssociateException.class, () -> associateService.updateAssociateDetails("cck@gmail.com", associateDto));
	}

	@Test
	void should_Delete_AssociateById() {
		Mockito.doNothing().when(associateRepository).deleteById(anyInt());
		associateService.removeAssociateById(anyInt());
		Mockito.verify(associateRepository).deleteById(anyInt());
	}
}