package com.epam.rd.gold.certificate.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.rd.gold.certificate.model.Associate;

public interface AssociateRepository extends JpaRepository<Associate, Integer> {
	List<Associate> findByGender(String gender);

	Optional<Associate> findByEmail(String email);
}
