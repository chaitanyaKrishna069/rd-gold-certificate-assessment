package com.epam.rd.gold.certificate.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.epam.rd.gold.certificate.custom.exception.AssociateException;
import com.epam.rd.gold.certificate.dto.AssociateDto;
import com.epam.rd.gold.certificate.dto.AssociateUpdateDto;
import com.epam.rd.gold.certificate.model.Associate;
import com.epam.rd.gold.certificate.model.Batch;
import com.epam.rd.gold.certificate.repository.AssociateRepository;
import com.epam.rd.gold.certificate.repository.BatchRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AssociateService {
	
	private final AssociateRepository associateRepository;
	private final BatchRepository batchRepository;
	private final ModelMapper modelMapper;
	
	public AssociateDto addAssociate(AssociateDto associateDto) {
		batchRepository.save(modelMapper.map(associateDto.getBatch(),Batch.class));
		Associate associate = modelMapper.map(associateDto, Associate.class);
		return modelMapper.map(associateRepository.save(associate), AssociateDto.class);
	}
	
	public List<AssociateDto> getAssociatesByGender(String gender) {
		return associateRepository.findByGender(gender).stream()
				.map(associate -> modelMapper.map(associate, AssociateDto.class)).toList();
	}
	
	public AssociateUpdateDto updateAssociateDetails(String email, AssociateUpdateDto associateUpdateDto) {
		Associate associate = associateRepository.findByEmail(email).orElseThrow(
				() -> {throw new AssociateException("The associate is not present");});
		modelMapper.map(associateUpdateDto, associate );
		return modelMapper.map(associateRepository.save(associate), AssociateUpdateDto.class);
	}
	
	public void removeAssociateById(int id) {
		associateRepository.deleteById(id);
	}
}