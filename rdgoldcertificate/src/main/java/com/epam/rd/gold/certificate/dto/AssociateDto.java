package com.epam.rd.gold.certificate.dto;

import com.epam.rd.gold.certificate.model.Batch;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AssociateDto {
	@NotBlank(message = "The name should not be empty")
	private String name;
	@NotBlank(message = "The mail should not be blank")
	@Email(message = "The mail should be proper format")
	private String email;
	@Pattern(regexp = "M|F", message = "Gender should be specified")
	private String gender;
	@NotBlank(message = "The college name should not be empty")
	private String college;
	@NotBlank(message = "The status should not be empty")
	private String status;
	private @Valid BatchDto batch;
}