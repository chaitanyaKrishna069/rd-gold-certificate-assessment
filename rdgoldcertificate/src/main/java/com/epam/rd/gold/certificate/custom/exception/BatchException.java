package com.epam.rd.gold.certificate.custom.exception;

public class BatchException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9131746142181022679L;

	public BatchException(String message) {
		super(message);
	}
}