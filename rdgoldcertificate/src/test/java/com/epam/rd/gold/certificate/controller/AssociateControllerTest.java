package com.epam.rd.gold.certificate.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.rd.gold.certificate.dto.AssociateDto;
import com.epam.rd.gold.certificate.dto.AssociateUpdateDto;
import com.epam.rd.gold.certificate.dto.BatchDto;
import com.epam.rd.gold.certificate.model.Associate;
import com.epam.rd.gold.certificate.model.Batch;
import com.epam.rd.gold.certificate.service.AssociateService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(AssociateController.class)
public class AssociateControllerTest {
	@Autowired
	MockMvc mockMvc;

	@MockBean
	private AssociateService associateService;
	
	private Batch batch;
	private BatchDto batchDto;
	private AssociateDto associateDto;
	private AssociateUpdateDto associateUpdateDto;
	private List<Associate> associates;

	@BeforeEach
	public void setUp() {
		batch = new Batch(1, "rt", "pp", "bla","bla");
		batchDto = new BatchDto( "string","string","21/02/2023","21/02/2023");
		associateDto = new AssociateDto("fg","r@gmail.com","M","pp","ACTIVE", batchDto);
		associateUpdateDto = new AssociateUpdateDto("fg","r@gmail.com","M","pp","ACTIVE");
		Associate associate = new Associate(0, "kl", "d@gmail.com", "pp","l","o", batch);
		associates = new ArrayList<>();
		associates.add(associate);
	}
	
	@Test
	void should_Get_Associates_ByGender() throws Exception {
		List<AssociateDto> associates = new ArrayList<>();
		associates.add(associateDto);
		Mockito.when(associateService.getAssociatesByGender("M")).thenReturn(associates);
		mockMvc.perform(get("/rd/associates/{gender}","M")).andExpect(status().isOk());
	}
	
	@Test
	void should_Create_Associate() throws Exception {
		Mockito.when(associateService.addAssociate(associateDto)).thenReturn(associateDto);
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonForm = objectMapper.writeValueAsString(associateDto);
		mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON).content(jsonForm))
		.andExpect(status().isCreated());
	}
	
	@Test
	void should_Update_Associate() throws Exception{
		Mockito.when(associateService.updateAssociateDetails("email@gmail.com",associateUpdateDto))
			.thenReturn(associateUpdateDto);
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonForm = objectMapper.writeValueAsString(associateDto);
		mockMvc.perform(put("/rd/associates").param("email", "cck@gmail.com")
				.contentType(MediaType.APPLICATION_JSON).content(jsonForm))
		.andExpect(status().isOk());
	}
	
	@Test
	void should_Delete_Associate() throws Exception{
		Mockito.doNothing().when(associateService).removeAssociateById(0);
		mockMvc.perform(delete("/rd/associates/{id}", "1")).andExpect(status().isOk());
	}
}